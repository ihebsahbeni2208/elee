package com.elee;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.ComponentScan;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(BinaryTree.class)
@ComponentScan({"com.elee"})

public class FindMaxTest {

    @Test
    public void should_ReturnTheMax_when_TreeIsNotNull() {
        BinaryTree tree = new BinaryTree();
        Node node = new Node(2);
        node.leftChild = new Node(22);
        node.rightChild = new Node(5);
        node.leftChild.rightChild = new Node(16);
        node.leftChild.rightChild.leftChild = new Node(1);
        node.leftChild.rightChild.rightChild = new Node(-5);
        node.rightChild.rightChild = new Node(9);
        node.rightChild.rightChild.leftChild = new Node(30);
        tree.root = node;

        int max = tree.findMax(tree.root);
        Assert.assertNotNull(max);
        Assert.assertEquals(30, max);
    }

    @Test
    public void should_ReturnMIN_VALUE_when_TreeIsNull() {
        BinaryTree tree = new BinaryTree();
        Node node = null;
        tree.root = node;

        int max = tree.findMax(tree.root);
        Assert.assertNotNull(max);
        Assert.assertEquals(Integer.MIN_VALUE, max);
    }
}
