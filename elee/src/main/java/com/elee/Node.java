package com.elee;

public class Node {
    int value;
    public Node leftChild;
    public Node rightChild;

    public Node (int value) {
        this.value = value;
        leftChild = null;
        rightChild = null;
    }
}
