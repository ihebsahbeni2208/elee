package com.elee;
public class BinaryTree {
    public Node root;
    public int findMax (Node root) {

        if (root == null) {
            return Integer.MIN_VALUE;
        }

        int max = Math.max(findMax(root.leftChild), findMax(root.rightChild));
        return max > root.value ? max : root.value;
    }

}
