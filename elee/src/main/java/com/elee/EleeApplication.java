package com.elee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EleeApplication {

	public static void main(String[] args) {
		SpringApplication.run(EleeApplication.class, args);

		BinaryTree tree = new BinaryTree();
		Node node = new Node(2);

		node.leftChild = new Node(7);
		node.rightChild = new Node(5);
		node.leftChild.rightChild = new Node(6);
		node.leftChild.rightChild.leftChild = new Node(1);
		node.leftChild.rightChild.rightChild = new Node(11);
		node.rightChild.rightChild = new Node(9);
		node.rightChild.rightChild.leftChild = new Node(4);
		tree.root = node;

		System.out.println("The max element of the binary tree is: "+ tree.findMax(tree.root));
	}

}
